// @flow

import { AppRegistry } from 'react-native'

import setup from './js/setup'

AppRegistry.registerComponent('R3Pi', setup)
