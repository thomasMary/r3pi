# R3Pi tech test

## Get it running
Follow the instructions from https://facebook.github.io/react-native/docs/getting-started.html

Run `yarn` to install the required dependencies

To run your app on iOS:
   `react-native run-ios`
   - or -
   Open `ios/R3Pi.xcodeproj` in Xcode
   Hit the Run button
To run your app on Android:
   Have an Android emulator running (quickest way to get started), or a device connected
   `react-native run-android`

## Scripts
codestyle: `npm run codestyle`
test: `npm run test` (todo)
