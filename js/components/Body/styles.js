// @flow

import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  header: {
    fontFamily: 'Helvetica',
    fontSize: 16,
    fontWeight: '400',
    color: 'white',
    paddingVertical: 10,
    backgroundColor: 'transparent',
  },
})
