// @flow

import React from 'react'
import { Text } from 'react-native'

import styles from './styles'

type Props = {
  children: any,
  style?: any,
}

const Body = ({
    children,
    style,
}: Props) => (
  <Text style={[styles.body, style]}>
    {children}
  </Text>
)

export default Body
