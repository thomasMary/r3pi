// @flow

import React from 'react'
import { Text } from 'react-native'

import styles from './styles'

type Props = {
  color?: string,
  children?: any,
  style?: any,
}

const Header = ({
  children = null,
  color = 'white',
  style = {},
}: Props) => (
  <Text style={[styles.header, { color }, style]}>
    {children}
  </Text>
)

export default Header
