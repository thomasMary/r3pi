// @flow

import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  header: {
    fontFamily: 'Helvetica',
    fontSize: 22,
    fontWeight: '500',
    color: 'white',
    paddingVertical: 10,
    backgroundColor: 'transparent',
  },
})
