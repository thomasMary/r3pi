// @flow

import React from 'react'
import { View, TouchableHighlight } from 'react-native'

import Header from '../Header'
import styles from './styles'

type Props = {
  children: any,
  onPress: () => Promise<any>,
}

const Button = ({
  children,
  onPress,
}: Props) => (
  <TouchableHighlight onPress={onPress} style={styles.buttonContainer}>
    <View>
      <Header>{children}</Header>
    </View>
  </TouchableHighlight>
)

export default Button
