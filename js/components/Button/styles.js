// @flow

import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  buttonContainer: {
    backgroundColor: 'rgb(255, 91, 98)',
    height: 40,
    alignSelf: 'stretch',
    margin: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
})
