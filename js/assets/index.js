
export const apples = require('./apples.jpg')
export const oranges = require('./oranges.jpg')
export const bananas = require('./bananas.jpg')
export const papaya = require('./papaya.jpg')

// icons
export const iconadd = require('./add.png')
export const iconsubstract = require('./sub.png')

// logos
export const r3pilogo = require('./r3pilogo.png')
