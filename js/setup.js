// @flow

import React from 'react'
import { Provider } from 'react-redux'

import configureStore from '@r3pi/redux/configure'

import App from './App'

type State = {
  store: any,
}

const setup = () => {
  class Root extends React.Component {
    state: State = {
      store: configureStore(),
    }

    render() {
      return (
        <Provider store={this.state.store}>
          <App />
        </Provider>
      )
    }
  }

  return Root
}

export default setup
