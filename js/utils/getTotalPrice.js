// @flow

import type { Item } from '@r3pi/types'

import getTotalItemPrice from './getTotalItemPrice'

const getTotalPrice = (items: Array<Item> = []): number => (
  items.reduce((total, item) => (
    total + getTotalItemPrice(item)
  ), 0)
)

export default getTotalPrice
