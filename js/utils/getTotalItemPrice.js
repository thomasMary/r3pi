// @flow

import type { Item } from '@r3pi/types'

const getTotalItemPrice = (item: Item): number => {
  if (item.discount === '3 for 2') {
    const discount = Math.floor(item.quantity / 3)

    return ((item.price * item.quantity) - (item.price * discount))
  }

  return (item.price * item.quantity)
}

export default getTotalItemPrice
