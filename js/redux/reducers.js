// @flow

import { combineReducers } from 'redux'

import navigation from './navigation'
import shopping from './shopping'

const reducers = combineReducers({
  navigation,
  shopping,
})

export default reducers
