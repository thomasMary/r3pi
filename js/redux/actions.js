// @flow

import * as NavigationActions from './navigation'
import * as ShoppingActions from './shopping'

const actions = {
  ...NavigationActions,
  ...ShoppingActions,
}

export default actions
