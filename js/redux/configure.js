// @flow

import { applyMiddleware, createStore, compose } from 'redux'

import clientMiddleware from './clientMiddleware'
import reducers from './reducers'

const configureStore = () => (
    createStore(
        reducers,
        compose(
            applyMiddleware(clientMiddleware),
        ),
    )
)

export default configureStore
