// @flow

import type { Item } from '@r3pi/types'

const initialState = []

const LOAD_ITEMS = 'LOAD_ITEMS'
const ADD_ITEM = 'ADD_ITEM'
const REMOVE_ITEM = 'REMOVE_ITEM'

const navigation = (state: Array<Item> = initialState, action: Object) => {
  console.log('action', action)
  switch (action.type) {
    case LOAD_ITEMS:
      return action.items
    case ADD_ITEM:
      const indexToAdd = state.findIndex(item => item.name === action.item.name)

      if (indexToAdd === -1) {
        return state
      }

      const newAddedItem = {
        ...state[indexToAdd],
        quantity: state[indexToAdd].quantity + 1,
      }

      return [
        ...state.slice(0, indexToAdd),
        newAddedItem,
        ...state.slice(indexToAdd + 1),
      ]
    case REMOVE_ITEM:
      const indexToRemove = state.findIndex(item => item.name === action.item.name)

      if (indexToRemove === -1) {
        return state
      }

      const newRemovedItem = {
        ...state[indexToRemove],
        quantity: (state[indexToRemove].quantity === 0 ? 0 : state[indexToRemove].quantity - 1),
      }

      return [
        ...state.slice(0, indexToRemove),
        newRemovedItem,
        ...state.slice(indexToRemove + 1),
      ]
    default:
      return state
  }
}

export const loadItems = () => ({
  type: LOAD_ITEMS,
  items: [
    { name: 'Apples', image: 'apples', price: 25, quantity: 0 },
    { name: 'Oranges', image: 'oranges', price: 30, quantity: 0 },
    { name: 'Bananas', image: 'bananas', price: 15, quantity: 0 },
    { name: 'Papaya', image: 'papaya', price: 50, quantity: 0, discount: '3 for 2' },
  ],
})

export const addItem = (item: Item) => ({
  type: ADD_ITEM,
  item,
})

export const removeItem = (item: Item) => ({
  type: REMOVE_ITEM,
  item,
})

export default navigation
