// @flow

const middleware = (store: Object) => (next: (action: Object) => any) => (action: Object): any => {
  if (!store) {
    return null
  }
  if (!action.promise || !action.types) {
    return next(action)
  }

  const [START, COMPLETED, FAILED] = action.types
  const dispatchAction = (actionType: string, data = {}) => {
    const newAction = {
      ...action,
      ...data,
      type: actionType,
    }

    delete newAction.promise
    next(newAction)
  }

  dispatchAction(START)

  return action.promise.then(
    result => dispatchAction(COMPLETED, { result }),
    error => dispatchAction(FAILED, { error }),
  )
}

export default middleware
