// @flow

import NavigationStateUtils from 'NavigationStateUtils'

const initialState = {
  index: 0,
  routes: [
    { key: 'Intro' },
  ],
}

const PUSH_SCREEN = 'PUSH_SCREEN'
const POP_SCREEN = 'POP_SCREEN'

const navigation = (state: Object = initialState, action: Object) => {
  switch (action.type) {
    case PUSH_SCREEN:
      return NavigationStateUtils.push(state, { key: action.screenName })
    case POP_SCREEN:
      return NavigationStateUtils.pop(state)
    default:
      return state
  }
}

export const pushScreen = (screenName: string) => ({
  type: PUSH_SCREEN,
  screenName,
})

export const popScreen = () => ({
  type: POP_SCREEN,
})

export default navigation
