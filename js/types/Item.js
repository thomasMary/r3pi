// @flow
/* eslint import/prefer-default-export: 0 */

export type Item = {
  name: string,
  image: string,
  price: number,
  quantity: number,
}
