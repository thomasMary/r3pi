// @flow

import React, { Component } from 'react'
import { NavigationExperimental } from 'react-native'
import { connect } from 'react-redux'

import * as screens from '@r3pi/screens'

const {
  CardStack: NavigationCardStack,
} = NavigationExperimental

type Props = {
  navigation: Object,
}

class App extends Component {
  props: Props

  renderScene = routeData => (
    React.createElement(screens[routeData.scene.route.key], routeData.passProps)
  )

  render() {
    const { navigation } = this.props

    return (
      <NavigationCardStack
        navigationState={navigation}
        renderScene={this.renderScene}
      />
    )
  }
}

const mapStateToProps = state => ({
  navigation: state.navigation || [],
})

export default connect(mapStateToProps)(App)
