// @flow

import React, { Component } from 'react'
import {
  View,
  Animated,
} from 'react-native'
import { connect } from 'react-redux'
import Animation from 'lottie-react-native'

import { watermelon } from '@r3pi/animations'
import actions from '@r3pi/redux'
import { Button, Header, Body } from '@r3pi/components'

import styles from './styles'

type Props = {
  goBack: () => Promise<any>,
}

type State = {
  progress: Animated.Value,
}

class Checkout extends Component {
  state: State = {
    progress: new Animated.Value(0),
  }

  componentDidMount() {
    Animated.timing(this.state.progress, {
      toValue: 1,
      duration: 5000,
    }).start()
  }
  props: Props

  render() {
    return (
      <View style={styles.container}>
        <Animation
          style={styles.animation}
          source={watermelon}
          progress={this.state.progress}
        />
        <View style={styles.textContainer}>
          <Header>Thanks</Header>
          <Body>And enjoy those fruits</Body>
        </View>
        <Button onPress={this.props.goBack}>
          CONTINUE SHOPPING
        </Button>
      </View>
    )
  }
}

const mapStateToProps = (state: Object) => ({
  items: state.shopping,
})

const mapDispatchToProps = dispatch => ({
  goBack: () => dispatch(actions.popScreen()),
})

export default connect(mapStateToProps, mapDispatchToProps)(Checkout)
