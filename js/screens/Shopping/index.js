// @flow

import React, { Component } from 'react'
import {
  View,
  Image,
  ListView,
} from 'react-native'
import { connect } from 'react-redux'

import { r3pilogo } from '@r3pi/assets'
import actions from '@r3pi/redux'
import { Button } from '@r3pi/components'
import type { Item } from '@r3pi/types'
import { getTotalPrice } from '@r3pi/utils'

import ItemRow from './ItemRow'
import styles from './styles'

type Props = {
  items: Array<Item>,
  loadItems: () => Promise<any>,
  removeItem: (item: Item) => Promise<any>,
  addItem: (item: Item) => Promise<any>,
  goToCheckout: () => Promise<any>,
}

type State = {
  datasource: ListView.DataSource,
}

class Shopping extends Component {
  constructor(props: Props) {
    super(props)

    this.state = {
      datasource: this.state.datasource.cloneWithRows(props.items),
    }
  }

  state: State = {
    datasource: new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2,
    }),
  }

  componentDidMount() {
    this.props.loadItems()
  }

  componentWillReceiveProps(nextProps: Props) {
    if (this.props.items !== nextProps.items) {
      this.setState({
        datasource: this.state.datasource.cloneWithRows(nextProps.items),
      })
    }
  }
  props: Props

  renderHeader = () => (
    <View style={styles.listHeader} />
  )

  renderSeparator = (sectionId: string, rowId: string) => (
    <View key={`${sectionId}-${rowId}`} style={styles.separator} />
  )

  renderRow = (item: Item) => (
    <ItemRow
      item={item}
      onPressLess={this.props.removeItem}
      onPressMore={this.props.addItem}
    />
  )

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <Image source={r3pilogo} style={styles.logo} />
        </View>
        <ListView
          dataSource={this.state.datasource}
          renderHeader={this.renderHeader}
          renderRow={this.renderRow}
          renderSeparator={this.renderSeparator}
          horizontal={false}
          enableEmptySections
          contentContainerStyle={styles.listContentContainer}
          style={styles.list}
        />
        <View style={styles.footer}>
          <Button onPress={this.props.goToCheckout}>CHECKOUT: £{getTotalPrice(this.props.items) / 100}</Button>
        </View>
      </View>
    )
  }
}

const mapStateToProps = (state: Object) => ({
  items: state.shopping,
})

const mapDispatchToProps = dispatch => ({
  loadItems: () => dispatch(actions.loadItems()),
  addItem: (item: Item) => dispatch(actions.addItem(item)),
  removeItem: (item: Item) => dispatch(actions.removeItem(item)),
  goToCheckout: () => dispatch(actions.pushScreen('Checkout')),
})

export default connect(mapStateToProps, mapDispatchToProps)(Shopping)
