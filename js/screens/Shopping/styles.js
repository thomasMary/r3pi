// @flow

import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  container: {
    flex: 1,
  },
  list: {
    flex: 1,
    flexDirection: 'column',
  },
  listContentContainer: {
    flex: 1,
  },
  listHeader: {
    height: 40,
    backgroundColor: 'lightgray',
    flexDirection: 'row',
  },
  header: {
    height: 100,
    backgroundColor: 'rgb(255, 91, 98)',
    justifyContent: 'center',
    alignItems: 'center',
  },
  logo: {
    height: 50,
    width: 100,
    marginTop: 20,
    resizeMode: 'contain',
  },
  footer: {
    height: 100,
    justifyContent: 'center',
    alignItems: 'center',
  },
  separator: {
    height: 5,
    backgroundColor: 'lightgray',
    flex: 1,
    flexDirection: 'row',
  },
})
