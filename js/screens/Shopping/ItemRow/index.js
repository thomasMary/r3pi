// @flow

import React from 'react'
import { View, Text, Image, TouchableOpacity } from 'react-native'

import type { Item } from '@r3pi/types'
import * as assets from '@r3pi/assets'
import { Header, Body } from '@r3pi/components'
import { getTotalItemPrice } from '@r3pi/utils'

import styles from './styles'

type Props = {
  item: Item,
  onPressMore: (item: Item) => Promise<any>,
  onPressLess: (item: Item) => Promise<any>,
}

const ItemRow = ({
  item,
  onPressMore,
  onPressLess,
}: Props) => {
  const pressLess = () => onPressLess(item)
  const pressMore = () => onPressMore(item)

  return (
    <View style={styles.container}>
      <View style={styles.product}>
        <Image source={assets[item.image]} style={styles.productImage} />
        <Body style={styles.productName} color="black">{item.name}</Body>
      </View>
      <View style={styles.quantity}>
        <TouchableOpacity onPress={pressLess} style={styles.buttonContainer}>
          <Image source={assets.iconsubstract} style={styles.buttonIcon} />
        </TouchableOpacity>
        <Header color="black">{item.quantity}</Header>
        <TouchableOpacity onPress={pressMore} style={styles.buttonContainer}>
          <Image source={assets.iconadd} style={styles.buttonIcon} />
        </TouchableOpacity>
      </View>
      <View style={styles.price}>
        <Text>£{getTotalItemPrice(item) / 100}</Text>
      </View>
    </View>
  )
}

export default ItemRow
