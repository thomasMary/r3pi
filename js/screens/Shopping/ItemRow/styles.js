// @flow

import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  container: {
    height: 80,
    margin: 0,
    backgroundColor: 'white',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonIcon: {
    width: 40,
    height: 40,
    resizeMode: 'cover',
    tintColor: 'green',
    marginHorizontal: 5,
  },
  productImage: {
    marginHorizontal: 20,
    height: 80,
    width: 80,
    resizeMode: 'contain',
  },
  productName: {
    alignSelf: 'center',
  },
  product: {
    flex: 1,
    flexDirection: 'row',
  },
  quantity: {
    width: 150,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  price: {
    width: 100,
    justifyContent: 'center',
    alignItems: 'center',
  },
})
